# Assign a float value to each card that will update the 'count' value
def calculate_float(card):
  if card in ['10', 'J', 'Q', 'K', 'A']:
    return -1
  elif card in ['2', '3', '4', '5', '6']:
    return 1
  else:
    return 0


# Figures out what the float value of the dealers card is by taking into account the current card count
def estimate_face_down_float(count):
  if count > 0:
    return -1
  elif count < 0:
    return 1
  else:
    return 0


# Calculate value of hand
def calculate_integer_value(cards):
  total = 0
  for card in cards:
    if card in ['J', 'Q', 'K']:
      total += 10
    elif card == 'A':
      if total + 11 <= 21:
        total += 11
      else:
        total += 1
    else:
      try:
        total += int(card)
      except ValueError:
        total += 10
  return total


# Making sure the visible_cards list is not wiped after every loop by placing it outside the loop function
def main():
  visible_cards = []

  # Input New cards in an infinite loop. splits individual cards that are inputted using split function. If position 0 in new_cards = exit then the loop breaks. Extend allows the list of cards to be saved through multiple itterations.
  while True:
    new_cards = input(
      "Enter additional visible cards separated by spaces (Use Capital Letters, type 'exit' to quit): "
    ).split()

    if new_cards[0].lower() == 'exit':
      break

    visible_cards.extend(new_cards)

    #First card is the dealer and Last two cards in the visible cards list are the players current cards. Assumes player sits at end of the dealing table.
    player_cards = visible_cards[-2:]
    dealer_face_up_card = visible_cards[0]

    # 'Count' calculation and values being parsed from others
    count = sum([calculate_float(card) for card in visible_cards])
    face_down_card = estimate_face_down_float(count)
    player_value = calculate_integer_value(player_cards)

    # Player Recommendations
    if player_value <= 11 and count != 0:
      print("Hit")
    elif player_value >= 17:
      print("Hold")
    elif player_value >= 11 and count > 0:
      print("Hold")
    else:
      print("Hit")

  # Print Player relevant Values on console
    print("Player Value:", player_value)
    print("Dealers Face Down card would have a float value of: ",
          face_down_card)


# Runs directly from the 'main()' function
if __name__ == "__main__":
  main()
